class Network {
    private int maskLength;
    private IPv4Address address;
    private IPv4Address mask;
    private long addressRange;
    private IPv4Address broadcast;
    private IPv4Address firstUsable;
    private IPv4Address lastUsable;

    public Network(IPv4Address address, int maskLength) throws IPv4exceptions {
        if ( maskLength > 32 || maskLength < 0 ) {
            throw new IllegalArgumentException();
        }
        this.maskLength = maskLength;
        this.addressRange = 1 << 32 - this.maskLength;

        this.mask = new IPv4Address(IPv4Address.MAXADDRESS - (this.addressRange - 1));
        this.address = new IPv4Address(address.toLong() - address.toLong() % this.addressRange);
        this.broadcast = new IPv4Address(this.address.toLong() + this.addressRange - 1);

        if ( this.addressRange > 2 ) {
            this.firstUsable = new IPv4Address(this.address.toLong() + 1);
            this.lastUsable = new IPv4Address(this.broadcast.toLong() - 1);
        } else {
            this.firstUsable = this.address;
            this.lastUsable = this.broadcast;
        }
    }
    
    public boolean contains(IPv4Address address) {
        if ( (address.greaterThan(this.address) && address.lessThan(this.broadcast)) || address.equals(this.address) || address.equals(this.broadcast) ){
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuffer address = new StringBuffer(18);

        address.insert(0, this.address.toString() + '/' + this.maskLength);

        return address.toString();
    }

    public IPv4Address getAddress() {
        return this.address;
    }

    public IPv4Address getBroadcastAddress() {
        return this.broadcast;
    }

    public IPv4Address getFirstUsableAddress() {
        return this.firstUsable;
    }

    public IPv4Address getLastUsableAddress() {
        return this.lastUsable;
    }
    
    public long getMask() {
        return this.mask.toLong();
    }

    public String getMaskString() {
        return this.mask.toString();
    }

    public int getMaskLength() {
        return this.maskLength;
    }

    public Network[] getSubnets() throws IPv4exceptions {
        if ( this.maskLength < 32 ) { 
            Network[] subnets = new Network[2];
            int length = maskLength + 1;

            subnets[0] = new Network(this.address, length); 
            subnets[1] = new Network(new IPv4Address(this.address.toLong() + this.addressRange / 2), length);

            return subnets;
        } else {
            return null;
        }
    }

    public long getTotalHosts() {
        if ( this.addressRange > 2 ) {
            return this.addressRange - 2;
        }
        return this.addressRange;        
    }

    public boolean isPublic() throws IPv4exceptions {
        Network[] privateNet = new Network[3];
        privateNet[0] = new Network(new IPv4Address("10.0.0.0"), 8);
        privateNet[1] = new Network(new IPv4Address("172.16.0.0"), 12);
        privateNet[2] = new Network(new IPv4Address("192.168.0.0"), 16);

        if ( privateNet[0].contains(this.address) || privateNet[1].contains(this.address) || privateNet[2].contains(this.address) ){
            return false;
        }
        return true;
    }
}