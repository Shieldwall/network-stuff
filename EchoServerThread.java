import java.net.*;
import java.io.*;

class EchoServerThread extends Thread {
    private Socket socket = null;

    public EchoServerThread(Socket socket) {
        this.socket = socket;
    }
    
    public void run() {
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String inputLine;
        
            while ((inputLine = in.readLine()) != null) {
                out.println(inputLine);
                if (inputLine.equals("disconnect\n")) {
                    break;
                }
            }
            in.close();
            out.close();
            socket.close();
        } catch(IOException e) {
            System.out.println("Something bad happened");
        }
    }
}