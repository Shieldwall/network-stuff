public class IPv4Address {
	private String strAddress;
	private long longAddress;

	public static final long MAXADDRESS = 4294967295L;
	
	public IPv4Address(String address) throws IPv4exceptions {
		this.strAddress = this.validateStr(address);
		this.longAddress = this.convert(address);
	}

	public IPv4Address(long address) throws IPv4exceptions {
		this.longAddress = this.validateLong(address);
		this.strAddress = this.convert(address);
	} 

	public String validateStr(String address) throws IPv4exceptions {
		String[] groups = address.split("\\.");

	    if ( groups.length != 4 ) {
	    	throw new IllegalArgumentException();
	    }

        for (int i = 0; i <= 3; i++) {
            String ipSegment = groups[i];
            int intSegment = 0;

            try {
                intSegment = Integer.parseInt(ipSegment);
            } catch(NumberFormatException e) {
                throw new IllegalArgumentException();
            }

            if ( intSegment > 255 || intSegment < 0 ) {
                throw new IllegalArgumentException();
            }
        }
	    return address;
	}

	public long validateLong(long address) throws IPv4exceptions {
		if ( address > MAXADDRESS || address < 0 ){
			throw new IllegalArgumentException();
		}

		return address;
	}

	public long convert(String address) {
		long result = 0;
 
		String[] groups = address.split("\\.");

		for (int i = 3; i >= 0; i--) {

			long ip = Long.parseLong(groups[3 - i]);

			result |= ip << (i * 8);

		}
 
		return result;
	}

	public String convert(long address) {
		StringBuffer result = new StringBuffer(15);
 
		for (int i = 0; i < 4; i++) {
	 
			result.insert(0,Long.toString(address & 0xff));
	 
			if (i < 3) {
				result.insert(0,'.');
			}
	 
			address = address >> 8;
		}

		return result.toString();
	}
 

	public boolean lessThan(IPv4Address address) {
		return this.longAddress < address.longAddress;
	}

	public boolean greaterThan(IPv4Address address) {
		return this.longAddress > address.longAddress;
	}

	public boolean equals(IPv4Address address) {
		return this.longAddress == address.longAddress;
	}

	public String toString() {
		return this.strAddress;
	}

	public long toLong() {
		return this.longAddress;
	}
}