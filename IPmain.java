import java.util.ArrayList;
import java.util.List;

public class IPmain {
    public static void main(String[] args)
        throws IPv4exceptions
    {
    	IPv4Address address = new IPv4Address("192.168.0.0");
		Network net = new Network(address, 24);
		Route addition = new Route(new Network(new IPv4Address("192.168.0.0"), 16), null, "added", 10);
		List<Route> routes = new ArrayList<Route>();
		
		routes.add(new Route(new Network(new IPv4Address("0.0.0.0"), 0), new IPv4Address("192.168.0.1"), "en0", 10));
		routes.add(new Route(new Network(new IPv4Address("192.168.0.0"), 24), null, "en0", 10));
		routes.add(new Route(new Network(new IPv4Address("10.0.0.0"), 8), new IPv4Address("10.123.0.1"), "en1", 10));
		routes.add(new Route(new Network(new IPv4Address("10.123.0.0"), 20), null, "en1", 10));

		Router router = new Router(routes);
		router.addRoute(addition);
		for (Route route : router.getRoutes()) {
			System.out.println(route.toString());
		}

		//System.out.println(route.toString());
	}       
}