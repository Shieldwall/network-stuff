import java.net.*;
import java.io.*;

class EchoServer {
    private int portNumber;
    private boolean canRun;
    private static final int DEFAULT_PORT = 55555;

    public EchoServer(int portNumber) throws IllegalArgumentException {
        if ( portNumber < 0 || portNumber > 65535 ) {
            throw new IllegalArgumentException();
        }
        this.canRun = false;
        this.portNumber = portNumber;
    }

    public EchoServer() {
        this.canRun = false;
        this.portNumber = DEFAULT_PORT;
    }
    
    public boolean isRunning() {
        return canRun;
    }

    public int getPort() {
        return this.portNumber;
    }

    public void start() throws IOException {
        this.canRun = true;
        ServerSocket serverSocket = new ServerSocket(portNumber);  
        while (canRun) {
            new EchoServerThread(serverSocket.accept()).start();
        }
    }

    public void stop() {
        this.canRun = false;
    }
}