import java.util.TreeSet;

class Router {
	private TreeSet<Route> routeList;

	public Router(Iterable<Route> routes) {
		this.routeList = new TreeSet<Route>(new RouteComparator());
		for ( Route route : routes ) {
			this.routeList.add(route);
		}
	}

	public void addRoute(Route route) {
		this.routeList.add(route);
	}

	public Route getRouteForAddress(IPv4Address address) {
		for (Iterator<Route> it = this.routeList.iterator(); it.hasNext(); ) {
        	Route current = it.next();

        	if ( current.getNetwork().contains(address) ) {
        	    return current;
        	} else {
        		return null;
        	}
    	}
	}

	public Iterable<Route> getRoutes() {
		return this.routeList;
	}

	public void removeRoute(Route route) {
		this.routeList.remove(route);
	}
}