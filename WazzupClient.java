import java.io.*;
import java.net.*;

class WazzupClient {
    public void connect(String hostName, int portNumber) throws IOException, UnknownHostException { 
    	Socket clientSocket = new Socket(hostName, portNumber);
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        String serverInput;
        String clientInput;

        out.println("Wazzup!");

        while ((serverInput = in.readLine()) != null) {
            System.out.println("Server: " + serverInput);
            
            clientInput = serverInput;
            if (clientInput != null) {
                System.out.println("Client: " + clientInput);
                out.println(clientInput);
            }
        }
        in.close();
        out.close();
        clientSocket.close();
    }
}