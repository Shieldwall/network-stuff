import java.util.Comparator;

class RouteComparator implements Comparator<Route> {
	public int compare(Route a, Route b) {
		if ( a.getNetwork().getMaskLength() == b.getNetwork().getMaskLength() ) {
			if ( a.getMetric() == b.getMetric() ) {
				return 0;
			} else if ( a.getMetric() > b.getMetric() ) {
				return 1;
			} else {
				return -1;
			}
		} else if ( a.getNetwork().getMaskLength() > b.getNetwork().getMaskLength() ) {
			return -1;
		} else {
			return 1;
		}
	}
}