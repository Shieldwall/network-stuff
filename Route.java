class Route {
	private Network network;
	private IPv4Address gateway;
	private String interfaceName;
	private int metric;

	public Route(Network network, IPv4Address gateway, String interfaceName, int metric) throws IPv4exceptions {
		this.network = network;
		this.gateway = gateway;
		this.interfaceName = interfaceName;
		if ( metric < 0 ) {
			throw new IllegalArgumentException();
		} else {
			this.metric = metric;
		}
	}

	public IPv4Address getGateway() {
		return this.gateway;
	}

	public String getinterfaceNameName() {
		return this.interfaceName;
	}

	public int getMetric() {
		return this.metric;
	}

	public Network getNetwork() {
		return this.network;
	}

	public String toString() {
		StringBuffer routeString = new StringBuffer();

        routeString.append("net: " + this.network.toString() + ", ");
        if ( gateway != null ) {
        	routeString.append("gateway: " + this.gateway.toString() + ", ");
        }
        routeString.append("interfaceName: " + this.interfaceName + ", metric: " + this.metric);

        return routeString.toString();
	}
}